/* ASCII Saver - Screensaver for terminals
 *
 * Copyright (C) 2019-2023 Mikhail E. Zakharov <zmey20000@yahoo.com>
 *
 * ASCII-animation files in the ascsaver.art directory are from collection
 * at http://artscene.textfiles.com/vt100/ neither author names nor copyright
 * restrictions are not known to me
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice immediately at the beginning of the file, without modification,
 *     this list of conditions, and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


/* -------------------------------------------------------------------------- */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <errno.h>
#include <paths.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pwd.h>
#include <termios.h>
#include <strings.h>
#include <sys/param.h>
#include <string.h>
#include <signal.h>

#ifdef __FreeBSD__
	#include <libutil.h>
#endif

#if defined(__OpenBSD__) || defined(__APPLE__) || defined(__NetBSD__)
#include <util.h>
#endif

#ifdef __linux__
	#include <pty.h>
	#include <utmp.h>
#endif


/* -------------------------------------------------------------------------- */
#define __PROGRAM	"ASCII Saver, v1.0.8.11"

/* Hide cursor */
#define HIDE_CURSOR()	fprintf(stdout, "\033[?25l"); fflush(stdout)
/* Show cursor */
#define SHOW_CURSOR()	fprintf(stdout, "\033[?25h"); fflush(stdout)
/* Clear screen */
#define CLS()		fprintf(stdout, "\033[1H\033[J"); fflush(stdout)
/* Reset graphic */
#define CLRR()		fprintf(stdout, "\033[0m"); fflush(stdout)
/* Set cursor position */
#define SET_POS(y, x)	fprintf(stdout, "\033[%d;%dH", y, x); fflush(stdout)
/* Get cursor position */
#define GET_POS(y, x)	fprintf(stdout, "\033[6n"); fflush(stdout); fscanf(stdin, "\033[%d;%dR", &y, &x)
/* Set screen size */
#define SCR_SIZE(y, x)	SET_POS(999,999); GET_POS(y, x)
/* ANSI ESC Save/Restore cursor position can be spoiled by art files.
Therefore using SET_POS/GET_POS
#define SCP()		printf("\033[s"); fflush(stdout)
#define RCP()		printf("\033[u"); fflush(stdout) */

#define STR_MAX		1024

static int master, slave;			/* PTY master/slave parts */
static struct termios tt;			/* Term capabilities */
static int pid;
FILE *sf;					/* ASCII show file */
int art_line = 0;
int eshow = 0;					/* Force embedded show */
int x, y, cx, cy;				/* Current and absolute X, Y */

/* -------------------------------------------------------------------------- */
static void err_exit(int ecode, const char *etext);
static void quit(int ecode);
static void trap(int sig);
static void wait4child(void);
int run_show(FILE *sf, char* cts, int rows);
static void usage();

/* -------------------------------------------------------------------------- */
int main(int argc, char *argv[]) {
	static struct termios rtt, stt;	/* Current terminal capabilities,
					temporary terminal capabilities */
	struct winsize win;
	fd_set	rfd;
	int n, cc;
	char buf[64 * STR_MAX];		/* Terminal IO buffer */
	time_t tvec, start;
	struct timeval tv;

	int ival = 1;			/* Start interval in secs */
	int speed = 64000;		/* Animation speed in microseconds */
	int pshow = 10;			/* Interval between 2 shows */
	int rows = 24;			/* Number of rows in a frame */
	int cls = 0;			/* Clear screen on the first run */

	char *show_file = NULL;		/* Full path to ASCII artfile to show */

	char scrbuf[STR_MAX * STR_MAX];	/* The screen buffer */
	int scrbufp = 0;		/* Offset in the screen buffer */
	int usescrbuf = 1;		/* Restore screen buffer or no */
	int stop_show = 0;		/* A flag to activite the show */
	int flg;			/* Command-line options flag */
	char *sh = NULL;		/* Shell to load */
	char cts[20];			/* Current time buffer */
	struct stat stb;


	if (argc < 2) {
		/* Show help if started without options */
		usage();
	 	exit(0);
	}

	while ((flg = getopt(argc, argv, "bcef:i:p:r:s:h")) != -1)
		switch(flg) {
			case 'b':
				usescrbuf = 0;	/* Don't use screen buffer */
				break;
			case 'c':
				cls = 1;	/* Clear screen on the first run */
				break;
			case 'e':
				eshow = 1;	/* Let it be undocumented ;-) */
				break;
			case 'f':
				if (optarg)
					show_file = optarg;
				break;
			case 'i':
				if ((ival = strtol(optarg, (char **)NULL, 10)) < 1) {
					(void)usage();
					err_exit(2, "FATAL: wrong [-i interval] value");
				}
				break;
			case 'p':
				if ((pshow = strtol(optarg, (char **)NULL, 10)) < 0) {
					(void)usage();
					err_exit(2, "FATAL: wrong [-p interval] value");
				}
				break;
			case 'r':
				if ((rows = strtol(optarg, (char **)NULL, 10)) < 0 || rows > 64) {
					(void)usage();
					err_exit(2, "FATAL: wrong [-r interval] value");
				}
				break;
			case 's':
				if ((speed = strtol(optarg, (char **)NULL, 10)) < 1) {
					(void)usage();
					err_exit(3, "FATAL: wrong [-s speed] value");
				}
				break;
			case 'h':
			default:
			 	(void)usage();
				exit(0);
		}

	if (eshow)
		fprintf(stderr, "INFO: Starting embedded show\n");
	else if (stat(show_file, &stb) == -1)
		fprintf(stderr, "WARNING: Unable to stat() ASCII art file: [%s]. Starting embedded show\n", show_file);
	else if (!S_ISREG(stb.st_mode))
		fprintf(stderr, "WARNING: ASCII art file: [%s] is not regular. Starting embedded show\n", show_file);
	else if (!(sf = fopen(show_file, "r")))
		fprintf(stderr, "WARNING: Unable to open() ASCII art file: [%s]. Starting embedded show\n", show_file);

	if (isatty(STDIN_FILENO)) {
		if (tcgetattr(STDIN_FILENO, &tt) == -1)
			err_exit(4, "Exception@tcgetattr()");

		if (ioctl(STDIN_FILENO, TIOCGWINSZ, &win) == -1)
			err_exit(5, "Exception@ioctl()");

		if (openpty(&master, &slave, NULL, &tt, &win) == -1)
			err_exit(6, "Exception@openpty()");
	} else
		err_exit(4, "Can run on terminals only");

	rtt = tt;
	(void)cfmakeraw(&rtt);
	rtt.c_lflag &= ~ECHO;
	(void)tcsetattr(STDIN_FILENO, TCSAFLUSH, &rtt);

	if ((pid = fork()) < 0) {		/* Error in fork() */
		fprintf(stderr, "FATAL: Exception@fork() %s\n", strerror(errno));
		(void)quit(7);
	}

	signal(SIGWINCH, trap);			/* Catch window change signal */

	if (pid == 0) {				/* Child spawns a shell */
		if (!(sh = getenv("SHELL"))) sh = _PATH_BSHELL;
		(void)close(master);
		login_tty(slave);
		execl(sh, sh, "-i", (char *)NULL);
		(void)exit(0);
	}

	/* Parent continues below */
	start = tvec = time(0);

	bzero(scrbuf, sizeof(scrbuf));
	GET_POS(cy, cx); SCR_SIZE(y, x); SET_POS(cy, cx);
	for (;;) {
		tv.tv_sec = 0;
		tv.tv_usec = speed;

		FD_SET(master, &rfd);
		FD_SET(STDIN_FILENO, &rfd);

		n = select(master + 1, &rfd, 0, 0, &tv);
		if (n < 0 && errno != EINTR)
			break;

		if (n > 0) {
			start = time(0);
			if (stop_show) {	/* Stop show on IO */
				CLRR();
				CLS();
				SHOW_CURSOR();
				if (scrbufp)
					write(STDOUT_FILENO, scrbuf + sizeof(scrbuf) - y*x - 2*y*x,
						y*x + 2*y*x);
			}

			/* INPUT */
			if (FD_ISSET(STDIN_FILENO, &rfd)) {
				cc = read(STDIN_FILENO, buf, sizeof(buf));
				/* Child is dead? Finish operations */
				if (cc < 0)
					break;

				/* Write EOF */
				if (cc == 0)
					if (tcgetattr(master, &stt) == 0 && (stt.c_lflag & ICANON) != 0)
						(void)write(master, &stt.c_cc[VEOF], 1);

				/* Write data to terminal */
				if (cc > 0) {
					if (!stop_show)
						(void)write(master, buf, cc);
					else {
						/* Drop current input buffer
						when leaving the show */
						bzero(buf, sizeof(buf));
						cc = 0;
						stop_show = 0;
					}
				}
			}

			/* OUTPUT */
			if (FD_ISSET(master, &rfd)) {
				if ((cc = read(master, buf, sizeof(buf))) > 0) {
					(void)write(STDOUT_FILENO, buf, cc);
					if (usescrbuf) {
						scrbufp = sizeof(scrbuf) - cc;
						memmove(scrbuf, scrbuf + cc, scrbufp);
						memmove(scrbuf + scrbufp, buf, cc);
					}
				} else
					break;
			}
		}

		/* Start the show if the time has come */
		tvec = time(0);
		if ((tvec - start) / 60  >= ival) {
			/* Clear screen & hide cursor before the first run */
			if (!stop_show && cls) {
				CLRR(); CLS();
			}
			HIDE_CURSOR();

			stop_show = 1;
			strftime(cts, 20, "%Y-%m-%d %H:%M:%S", localtime(&tvec));
			if (run_show(sf, cts, rows))
				if (sf)	/* A short break between shows, and
					no break for the embedded one */
					start = tvec - (ival * 60) + pshow;
		}
	}

	(void)wait4child();
	(void)quit(0);
}

/* -------------------------------------------------------------------------- */
static void err_exit(int ecode, const char *etext) {
	(void)fprintf(stderr, "FATAL: %s: %s\n", etext, strerror(errno));
	(void)exit(ecode);
}

/* -------------------------------------------------------------------------- */
static void quit(int ecode) {
	(void)tcsetattr(STDIN_FILENO, TCSAFLUSH, &tt);	/* Restore TTY */
	(void)close(master);
	if (sf)
		(void)fclose(sf);
	(void)exit(ecode);
}

/* -------------------------------------------------------------------------- */
static void trap(int sig) {
	struct winsize win;

	/* We hook change window change only signal, but let's keep switch/case
	construction for future */
	switch(sig) {
		case SIGWINCH:
			/* Terminal resized -> re-size slave */
			if (ioctl(STDIN_FILENO, TIOCGWINSZ, &win) != -1) {
				ioctl(slave, TIOCSWINSZ, &win);
				GET_POS(cy, cx); SCR_SIZE(y, x); SET_POS(cy, cx);
				CLRR();
			}
			break;
		default:
			break;
	}
}

/* -------------------------------------------------------------------------- */
static void wait4child(void) {
	int e, status;

	if (waitpid(pid, &status, 0) == pid) {
		if (WIFEXITED(status))
			e = WEXITSTATUS(status);
		else if (WIFSIGNALED(status))
			e = WTERMSIG(status);
		else /* can't happen */
			e = 1;
		(void)quit(e);
	}
}

/* -------------------------------------------------------------------------- */
int run_show(FILE *sf, char* cts, int rows) {
	#define arth 24
	#define artw 80
	char show_art[arth][artw] = {
	"\033[33;40m+-------.'7A;.-----------------------.:6D'.--------+",
	"|      .;657932.                    .303030;.      |",
	"|    .;304079616.                  .86F6F2E63;.    |",
	"|   .6F6D20323031.                .392E31322E30.   |",
	"|  '37207A6D657932,              ,30303030407961'  |",
	"| ,686F6F2E636F6D20;            ;323031392E31322E, |",
	"|.3037207A6D65793230:.        .:303030407961686F6F.|",
	".2E636F6D20323031392E.        .31322E3037207A6D6579.",
	"'323030303040796168,. .;6F6F;. .,2E636F6D2032303139'",
	";2E31322E30372 \033[31m+---------------------+\033[33m 793230303030;",
	"'407961686F6F2 \033[31m|    DO NOT PANIC!    |\033[33m 20323031392E'",
	"|              \033[31m| %s |\033[33m |",
	"|              \033[31m+---------------------+\033[33m             |",
	"|                      ......                      |",
	"|                   .:37207A6D:.                   |",
	"|                  .657932303030.                  |",
	"|                 '30407961686F6F'                 |",
	"|                '2E636F6D20323031,                |",
	"|               ,392E31322E3037207A,               |",
	"|              ;6D657932303030304079;              |",
	"|            .:61686F6F2E636F6D203230:.            |",
	"|            '31392E31322E3037207A6D65'            |",
	"|             .'79323030303040796168'.             |",
	"+-------------------...6F6F2E...-------------------+"};

	char show_art_warn[] = "\033[91mThe terminal window is too small!";
	char s[STR_MAX];
	int show_end = 0;		/* 1 if the show reached the end */
	int h;

	if (x < artw || y < arth) {
		SET_POS(1,1);
		fprintf(stdout, "%s", show_art_warn);
		show_end = 1;
	} else
		if (sf && !eshow) {		/* Print an ASCI Art file */
			for (h = 0; h < rows; h++)
				if (fgets(s, STR_MAX, sf)) {
					fprintf(stdout, "%s\r", s);
				} else {
					rewind(sf);
					show_end = 1;
					break;
				}
		} else {			/* Play Embedded show */
			for (h = 0; h <= arth; h++)
				SET_POS(y/2  - 11 + art_line, x/2 - 26);

				if (art_line == 11) {
					fprintf(stdout, "|              \033[31;40m| %s |\033[33m             |", cts);
				} else {
					fprintf(stdout, "%s", show_art[art_line]);
				}

				if (art_line++ == arth - 1) {
					art_line = 0;
					show_end = 1;
				}
		}

	fflush(stdout);
	return(show_end);
}

/* -------------------------------------------------------------------------- */
void usage(void) {
	printf("%s\n\nUsage:\n\
\tascsaver [-b][-i n][-p n][-r n][-s n][-f ascii.art][-h]\n\n\
[-b]\t\tDo not attempt to restore the screen after show\n\
[-c]\t\tClear screen before starting a show\n\
[-i n]\t\tWait n minutes (default 1) before running a screensaver show\n\
[-p n]\t\tPause between shows in seconds, default 10\n\
[-r n]\t\tNumber of rows in an animation frame, default 24\n\
[-s n]\t\tAnimation speed n in microseconds, default 64000\n\
[-f ascii.art]\tA file with ASCII art animation\n\
[-h]\t\tThis message\n\n", __PROGRAM);
}
