#!/usr/bin/env python3

"""
A simple VT100 ASCII art animation player

----------------------------------------------------------------------------
"THE BEER-WARE LICENSE" (Revision 42):
<zmey20000@yahoo.com> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.    Mikhail Zakharov
----------------------------------------------------------------------------
"""

import sys
import time


# --------------------------------------------------------------------------
def quit(e=0):
  print('\033[?25h')
  sys.exit(e)

# --------------------------------------------------------------------------
argc = len(sys.argv)

if argc == 1:
  print(f'Usage:\n\t{sys.argv[0]} file.vt|- [rate|0.0002]')
  sys.exit(0)

print('\033[?25l')
f = open(sys.argv[1], 'r') if argc > 1 and sys.argv[1] != '-' else sys.stdin
while ch := f.read(1):
  try:
    print(ch, end ='')
    time.sleep(float(sys.argv[2]) if argc == 3 else 0.0002)
  except KeyboardInterrupt:
    quit(1)

if f != sys.stdin:
  f.close()

quit()
