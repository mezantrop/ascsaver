# ASCII Saver - Screensaver for terminals

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

This is my attempt to run some animation while there is no I/O on a terminal. It
runs on FreeBSD and macOS, should also work on Linux.

If you are more interested in a screenlock than a screensaver, check [sclocka](https://github.com/mezantrop/sclocka).

[![ascserver](https://gitlab.com/mezantrop/ascsaver/raw/master/globe.gif)](https://gitlab.com/mezantrop/ascsaver/raw/master/globe.gif)

## Live action

You can find `ASCII Saver` running in [Slovenian Computer History Museum](https://www.racunalniski-muzej.si/) on

Gorenje Delta Paka 3000 (Terminal made in Slovenia in Yugoslavia, supports VT100 emulation)

![paka3000](/uploads/5189be938a82e7f078295d63ce7bb3d1/paka3000.gif)

DEC VT320

![vt320](/uploads/99b64a3dd8d0cc0ca51237d321b59d43/vt320.gif)

## Build

```sh
$ make
```

## Install

```sh
$ make install clean
```

## Run

```sh
$ ascsaver -f /usr/local/share/ascsaver.art/star_wars.vt -s 128000 -r 14
```

And don't touch the keyboard for a minute to see the Star Wars episode IV running in your terminal!

### Options

```sh
ASCII Saver, v1.0.8.11

Usage:
	ascsaver [-b][-i n][-p n][-r n][-s n][-f ascii.art][-h]

[-b]		Do not attempt to restore the screen after show
[-c]		Clear screen before starting a show
[-i n]		Wait n minutes (default 1) before running a screensaver show
[-p n]		Pause between shows in seconds, default 10
[-r n]		Number of rows in an animation frame, default 24
[-s n]		Animation speed n in microseconds, default 64000
[-f ascii.art]	A file with ASCII art animation
[-h]		This message
```

### Other interesting invocations

```sh
$ ascsaver -f /usr/local/share/ascsaver.art/globe.vt -p 0
```

Also, if you are an adult, dogs.vt looks very nice:

```sh
$ ascsaver -f /usr/local/share/ascsaver.art/dogs.vt -r 1 -p 5 -s 32000
```

Otherwise, better see the inspiring nasa.vt animation:

```sh
$ ascsaver -f /usr/local/share/ascsaver.art/nasa.vt -s 128000

```

The [ascsaver.art](https://gitlab.com/mezantrop/ascsaver/tree/master/ascsaver.art)
directory contains several examples of good old school ASCII-art animation files
from the [Artscene](http://artscene.textfiles.com/vt100/) collection, as well as
a dump of Star Wars episode IV telnet session from towel.blinkenlights.nl:23

### Contributors

* Chris Pinnock https://chrispinnock.com - NetBSD/OpenBSD portability improvements

## VT100 animation player

`vt_play.py` is a simple Python 3 based player for VT100 animation files

```sh
$ vt_play.py

Usage:
        vt_play.py file.vt|- [rate|0.0002]
```
