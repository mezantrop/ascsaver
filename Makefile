CC=cc
NAME=ascsaver
LIBS=-lutil
PREFIX = /usr/local

all:
	${CC} -Wall -o ${NAME} ascsaver.c ${LIBS}

install: all
	[ -f `which strip` ] && strip ${NAME}
	[ -d ${PREFIX}/bin ] && cp ${NAME} ${PREFIX}/bin || mkdir -p ${PREFIX}/bin && cp ${NAME} ${PREFIX}/bin
	[ -d ${PREFIX}/share/${NAME}.art ] && cp ${NAME}.art/* ${PREFIX}/share/${NAME}.art/ || mkdir -p ${PREFIX}/share/${NAME}.art && cp ${NAME}.art/* ${PREFIX}/share/${NAME}.art/

deinstall:
	rm -f ${PREFIX}/bin/${NAME}
	rm -rf ${PREFIX}/share/${NAME}.art
uninstall:	deinstall

clean:
	rm -f ${NAME}
